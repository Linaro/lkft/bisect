#!/bin/bash

# Dependencies:
# see https://gitlab.com/Linaro/tuxmake
#
# You have to stand in the kernel tree to run this script.
# take whatever is in the tuxmake_reproducer.sh and pass that after this script 'bisect.sh'.
# NEW=next-20201210 OLD=v5.10-rc7-21-g69fe24d1d80f bisect.sh tuxmake -q --build-dir=$(pwd)/bisect-build-output --runtime docker --target-arch sh --toolchain gcc-10 --kconfig defconfig

TUXMAKE_CMD="${TUXMAKE_CMD:-"$@"}"
export TUXMAKE_IMAGE_REGISTRY=public.ecr.aws


if [[ -z "${NEW}" ]]; then
	echo "need to set NEW"
	exit 1
fi

if [[ -z "${OLD}" ]]; then
	echo "need to set OLD"
	exit 1
fi

NEW_SHA=$(git rev-parse ${NEW})
OLD_SHA=$(git rev-parse ${OLD})

echo "Start bisecting..."
git checkout ${NEW_SHA}
git bisect start ${NEW_SHA} ${OLD_SHA}

git bisect run $TUXMAKE_CMD 2>&1| tee bisect_output.log
echo
echo
echo "Bisect log:"
echo
git bisect log|tee bisect_${NEW_SHA}-${OLD_SHA}.log
first_bad_commit=$(cat "bisect_${NEW_SHA}-${OLD_SHA}.log"|tail -1|sed 's/.*\[//'|sed 's/\].*//')
echo
echo
echo "The first bad commit:"
echo
git log -1 --stat $first_bad_commit| tee -a "bisect_${NEW_SHA}-${OLD_SHA}.log"
echo $first_bad_commit > .first_bad_commit
git bisect reset
