#!/bin/bash

set +e

TUXMAKE_CMD="${TUXMAKE_CMD:-"$@"}"
export TUXMAKE_IMAGE_REGISTRY=public.ecr.aws

echo
echo
echo "Reverting the bad commit: $FIRST_BAD_COMMIT"
echo
git revert --no-edit $FIRST_BAD_COMMIT
if [[ $? -ne 0 ]]; then
	touch revert_output.log
	exit 0
fi
$TUXMAKE_CMD 2>&1| tee revert_output.log
