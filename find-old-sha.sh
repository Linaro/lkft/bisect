#!/bin/bash

set -x

# If we're here (or beyond) we've reached
# too far and should stop.
COUNT_HARD_LIMIT="${COUNT_HARD_LIMIT:-16384}"

OLDSHA_FILE="${OLDSHA_FILE:-".oldsha"}"

if [[ ${BISECT_COUNT} -eq 0 ]]; then
	echo "Aborting, BISECT_COUNT is set to 0"
	exit 1
fi

if [[ -z "${OLD}" ]]; then

	echo "trying to find the old"
	count=${BISECT_COUNT}
	found_good_old=0

	while true; do
		count=$((${count}*2))
		if [[ ${count} -ge ${COUNT_HARD_LIMIT} ]]; then
			echo "Reached max count limit (${COUNT_HARD_LIMIT}). Exiting."
			exit 1
		fi
		OLD=$(git log -1 --format=format:%H ${NEW}~${count})
		git checkout ${OLD}
		$TUXMAKE_CMD
		if [[ $? -eq 0 ]]; then
			break
		fi
	done
else
	# if OLD is set, it can be a git tag, branch, or sha.
	# Ex: For linux-next, use origin/stable
	OLD=$(git rev-parse "${OLD}")
fi
echo "$OLD" > ${OLDSHA_FILE}
